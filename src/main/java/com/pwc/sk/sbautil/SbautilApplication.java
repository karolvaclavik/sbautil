package com.pwc.sk.sbautil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbautilApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbautilApplication.class, args);
    }

}
