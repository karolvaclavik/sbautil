/*
 * Copyright 2020 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pwc.sk.sbautil.optaplanner.domain;


import com.pwc.sk.sbautil.core.optimization.DistanceProvider;
import com.pwc.sk.sbautil.model.basedata.CashCenter;
import com.pwc.sk.sbautil.model.optimization.CashPoint;
import com.pwc.sk.sbautil.model.optimization.Center;
import lombok.Data;
import org.optaplanner.core.api.domain.constraintweight.ConstraintConfigurationProvider;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import static java.util.Collections.emptyList;


@Data
@Component
@PlanningSolution
public class FacilityLocationProblem {

    @Autowired
    DistanceProvider distanceProvider;

    @ProblemFactCollectionProperty
    @ValueRangeProvider(id = "ccRange")
    private List<Center> centers;
    @PlanningEntityCollectionProperty
    private List<CashPoint> cashPoints;

    @PlanningScore
    private HardSoftLongScore score;
    @ConstraintConfigurationProvider
    private FacilityLocationConstraintConfiguration constraintConfiguration = new FacilityLocationConstraintConfiguration();


    public FacilityLocationProblem() {
    }

    public FacilityLocationProblem(
            List<Center> centers,
            List<CashPoint> cashPoints
            ) {
        this.centers = centers;
        this.cashPoints = cashPoints;
    }

    public static FacilityLocationProblem empty() {
        FacilityLocationProblem problem = new FacilityLocationProblem(
                emptyList(),
                emptyList());
        problem.setScore(HardSoftLongScore.ZERO);
        return problem;
    }


    public double getTotalCost() {
        double result = 0;
        for (Center c : centers) {
            if (c.isUsed()) {
                result = result + c.getSetupCost();
            }
        }
        return result;
    }

    public double getPotentialCost() {
        double result = 0;
        for (Center c : centers) {
            result = result + c.getSetupCost();
        }
        return result;
    }

    public String getTotalDistance() {
        double result = 0;
        for (CashPoint cp : this.cashPoints) {
            if (cp.isAssigned()) {
                distanceProvider.getDistance(null,cp.getP());
            }
        }
        return result / 1000 + " km";
    }

    @Override
    public String toString() {
        return "FacilityLocationProblem{" +
                "centers: " + centers.size() +
                ", cashPoints: " + cashPoints.size() +
                ", score: " + score +
                '}';
    }
}
