package com.pwc.sk.sbautil.model.geoservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Datum {
    @JsonProperty("latitude")
    double latitude;

    @JsonProperty("longitude")
    double longitude;

    @JsonProperty("type")
    String type;

    @JsonProperty("name")
    String name;

    @JsonProperty("number")
    String number;

    @JsonProperty("postal_code")
    String postal_code;

    @JsonProperty("street")
    String street;

    @JsonProperty("map_url")
    String map_url;

    @JsonProperty("confidence")
    double confidence;

    @JsonProperty("region")
    String region;

    @JsonProperty("region_code")
    String region_code;

    @JsonProperty("county")
    String county;

    @JsonProperty("locality")
    String locality;

    @JsonProperty("administrative_area")
    Object administrative_area;

    @JsonProperty("neighbourhood")
    Object neighbourhood;

    @JsonProperty("country")
    String country;

    @JsonProperty("country_code")
    String country_code;

    @JsonProperty("continent")
    String continent;

    @JsonProperty("label")
    String label;

    @JsonProperty("bbox_module")
    List<Object> bbox_module;

    @JsonProperty("country_module")
    CountryModule country_module;

    @JsonProperty("sun_module")
    SunModule sun_module;

    @JsonProperty("timezone_module")
    TimezoneModule timezone_module;
}