package com.pwc.sk.sbautil.model.basedata;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "variant")
public class Variant {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "c1")
    private Long c1;

    @Column(name = "c2")
    private Long c2;

    @Column(name = "c3")
    private Long c3;

    @Column(name = "c4")
    private Long c4;

    @Column(name = "c5")
    private Long c5;

    @Column(name = "c6")
    private Long c6;

    @Column(name = "c7")
    private Long c7;

    @Column(name = "c8")
    private Long c8;

    @Column(name = "c9")
    private Long c9;

    @Column(name = "total")
    private Double total;

    @Column(name="distance")
    private Double distance;

}
