package com.pwc.sk.sbautil.model.googledistancematrix;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Row {

    @JsonProperty("elements")
    List<Element> elements;

}
