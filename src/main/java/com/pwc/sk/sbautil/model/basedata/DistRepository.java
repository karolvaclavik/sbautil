package com.pwc.sk.sbautil.model.basedata;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DistRepository extends JpaRepository<Dist, Long> {

}
