package com.pwc.sk.sbautil.model.basedata;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CashCenterRepository extends JpaRepository<CashCenter, Long> {

}
