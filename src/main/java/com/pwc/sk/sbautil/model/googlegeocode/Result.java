package com.pwc.sk.sbautil.model.googlegeocode;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Result {
    @JsonProperty("address_components")
    List<AddressComponent> address_components;

    @JsonProperty("formatted_address")
    String formatted_address;

    @JsonProperty("geometry")
    Geometry geometry;

    @JsonProperty("place_id")
    String place_id;

    @JsonProperty("plus_code")
    PlusCode plus_code;

    @JsonProperty("types")
    List<String> types;

    @JsonProperty("partial_match")
    boolean partial_match;
}
