package com.pwc.sk.sbautil.model.geoservice;

import lombok.Data;

@Data
public class Geolocation {

    private double latitude;
    private double longitude;
    private String rawData;

    public Geolocation(double latitude, double longitude, String rawData) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.rawData = rawData;
    }

}
