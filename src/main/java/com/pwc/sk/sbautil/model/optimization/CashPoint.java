package com.pwc.sk.sbautil.model.optimization;

import com.pwc.sk.sbautil.core.optimization.DistanceProvider;
import com.pwc.sk.sbautil.model.basedata.Point;
import lombok.Data;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@Data
@PlanningEntity
public class CashPoint {

    private Long id;
    private long demand;
    private DistanceProvider distanceProvider;

    private Point p;

    @PlanningVariable(valueRangeProviderRefs = "ccRange")
    private Center center;

    public boolean isAssigned() {
        return center != null;
    }

    public CashPoint(Long id, long demand, Point p, DistanceProvider distanceProvider) {
        this.id = id;
        this.demand = demand;
        this.distanceProvider = distanceProvider;
        this.p = p;
    }

    public CashPoint() {

    }

    public long distanceFromCenter() {
        if (center == null) {
            throw new IllegalStateException("No center is assigned.");
        }
        return distanceProvider.getDistance(null,this.getP());
    }

}
