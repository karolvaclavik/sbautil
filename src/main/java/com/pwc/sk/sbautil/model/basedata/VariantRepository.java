package com.pwc.sk.sbautil.model.basedata;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VariantRepository extends JpaRepository<Variant, Long> {

}
