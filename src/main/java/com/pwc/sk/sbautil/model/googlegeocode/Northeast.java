package com.pwc.sk.sbautil.model.googlegeocode;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Northeast {

    @JsonProperty("lat")
    double lat;

    @JsonProperty("lng")
    double lng;

}
