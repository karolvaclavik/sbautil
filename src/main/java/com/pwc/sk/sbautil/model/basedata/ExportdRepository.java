package com.pwc.sk.sbautil.model.basedata;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExportdRepository extends JpaRepository<Exportd, Long> {

}
