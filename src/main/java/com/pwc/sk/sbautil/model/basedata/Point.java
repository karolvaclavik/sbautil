package com.pwc.sk.sbautil.model.basedata;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "point", schema = "public")
public class Point {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "bank")
    private String bank;

    @Column(name = "street")
    private String street;

    @Column(name = "city")
    private String city;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "atm")
    private Long atm;

    @Column(name = "googledata")
    private String googleData;

    @Column(name = "gdistance")
    private String gdistance;

    @Column(name = "gadress")
    private String gadress;


}
