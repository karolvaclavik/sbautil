package com.pwc.sk.sbautil.model.googlegeocode;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlusCode {

    @JsonProperty("compound_code")
    String compound_code;

    @JsonProperty("global_code")
    String global_code;

}
