package com.pwc.sk.sbautil.model.basedata;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "dist")
public class Dist {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "point")
    private Long point;

    @Column(name = "cc")
    private Long cc;

    //distance from point co cash center in meters
    @Column(name = "distance")
    private Double distance;

    //distance from point co cash center in km - textual representation
    @Column(name = "distance_text")
    private String distanceText;

    //time from point to cash center in seconds
    @Column(name = "time")
    private Double time;

    //time from point to cash center - textual representation
    @Column(name = "time_text")
    private String timeText;

}
