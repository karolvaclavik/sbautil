package com.pwc.sk.sbautil.model.googledistancematrix;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Element {

    @JsonProperty("distance")
    Distance distance;

    @JsonProperty("duration")
    Duration duration;

    @JsonProperty("status")
    String status;

}
