package com.pwc.sk.sbautil.model.basedata;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "point_geodata")
public class PointGeodata {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "content")
    private String content;

    @Column(name = "confidence")
    private Double confidence;

    @Column(name = "country")
    private String country;

    @Column(name = "svkconfidence")
    private Double svkconfidence;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "type")
    private String type;

    public PointGeodata() {}

    public PointGeodata(long id, String content) {
        this.id = id;
        this.content = content;
    }

}
