package com.pwc.sk.sbautil.model.googledistancematrix;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Distance {

    @JsonProperty("text")
    String text;

    @JsonProperty("value")
    int value;

}
