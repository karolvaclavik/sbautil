package com.pwc.sk.sbautil.model.googlegeocode;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class AddressComponent {

    @JsonProperty("long_name")
    String long_name;

    @JsonProperty("short_name")
    String short_name;

    @JsonProperty("types")
    List<String> types;

}
