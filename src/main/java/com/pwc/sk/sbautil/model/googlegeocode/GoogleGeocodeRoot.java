package com.pwc.sk.sbautil.model.googlegeocode;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class GoogleGeocodeRoot {

    @JsonProperty("results")
    List<Result> results;

    @JsonProperty("status")
    String status;

}
