package com.pwc.sk.sbautil.model.googlegeocode;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Geometry {

    @JsonProperty("bounds")
    Bounds bounds;

    @JsonProperty("location")
    Location location;

    @JsonProperty("location_type")
    String location_type;

    @JsonProperty("viewport")
    Viewport viewport;

}
