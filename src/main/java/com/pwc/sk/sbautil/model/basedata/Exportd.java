package com.pwc.sk.sbautil.model.basedata;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "exportd")
public class Exportd {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "bank")
    private String bank;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "atm")
    private Long atm;

    @Column(name = "t1")
    private Double t1;

    @Column(name = "t2")
    private Double t2;

    @Column(name = "t3")
    private Double t3;

    @Column(name = "t4")
    private Double t4;

    @Column(name = "t5")
    private Double t5;

    @Column(name = "t6")
    private Double t6;

    @Column(name = "t7")
    private Double t7;

    @Column(name = "t8")
    private Double t8;

    @Column(name = "t9")
    private Double t9;

    @Column(name = "address")
    private String address;

}
