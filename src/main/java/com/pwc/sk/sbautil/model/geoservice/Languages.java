package com.pwc.sk.sbautil.model.geoservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Languages {

    @JsonProperty("bar")
    public String getBar() {
        return this.bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }

    String bar;


    @JsonProperty("urd")
    public String getUrd() {
        return this.urd;
    }

    public void setUrd(String urd) {
        this.urd = urd;
    }

    String urd;


    @JsonProperty("zho")
    public String getZho() {
        return this.zho;
    }

    public void setZho(String zho) {
        this.zho = zho;
    }

    String zho;

    @JsonProperty("nzs")
    public String getNzs() {
        return this.nzs;
    }

    public void setNzs(String nzs) {
        this.nzs = nzs;
    }

    String nzs;


    @JsonProperty("mri")
    public String getMri() {
        return this.mri;
    }

    public void setMri(String mri) {
        this.mri = mri;
    }

    String mri;

    @JsonProperty("deu")
    public String getDeu() {
        return this.deu;
    }

    public void setDeu(String deu) {
        this.deu = deu;
    }

    String deu;


    @JsonProperty("swe")
    public String getSwe() {
        return this.swe;
    }

    public void setSwe(String slk) {
        this.swe = swe;
    }

    String swe;

    @JsonProperty("slk")
    public String getSlk() {
        return this.slk;
    }

    public void setSlk(String slk) {
        this.slk = slk;
    }

    String slk;

    @JsonProperty("afr")
    public String getAfr() {
        return this.afr;
    }

    public void setAfr(String afr) {
        this.afr = afr;
    }

    String afr;

    @JsonProperty("eng")
    public String getEng() {
        return this.eng;
    }

    public void setEng(String eng) {
        this.eng = eng;
    }

    String eng;

    @JsonProperty("nbl")
    public String getNbl() {
        return this.nbl;
    }

    public void setNbl(String nbl) {
        this.nbl = nbl;
    }

    String nbl;

    @JsonProperty("nso")
    public String getNso() {
        return this.nso;
    }

    public void setNso(String nso) {
        this.nso = nso;
    }

    String nso;

    @JsonProperty("sot")
    public String getSot() {
        return this.sot;
    }

    public void setSot(String sot) {
        this.sot = sot;
    }

    String sot;

    @JsonProperty("ssw")
    public String getSsw() {
        return this.ssw;
    }

    public void setSsw(String ssw) {
        this.ssw = ssw;
    }

    String ssw;

    @JsonProperty("tsn")
    public String getTsn() {
        return this.tsn;
    }

    public void setTsn(String tsn) {
        this.tsn = tsn;
    }

    String tsn;

    @JsonProperty("tso")
    public String getTso() {
        return this.tso;
    }

    public void setTso(String tso) {
        this.tso = tso;
    }

    String tso;

    @JsonProperty("ven")
    public String getVen() {
        return this.ven;
    }

    public void setVen(String ven) {
        this.ven = ven;
    }

    String ven;

    @JsonProperty("xho")
    public String getXho() {
        return this.xho;
    }

    public void setXho(String xho) {
        this.xho = xho;
    }

    String xho;

    @JsonProperty("zul")
    public String getZul() {
        return this.zul;
    }

    public void setZul(String zul) {
        this.zul = zul;
    }

    String zul;

}
