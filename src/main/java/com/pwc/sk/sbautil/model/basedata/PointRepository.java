package com.pwc.sk.sbautil.model.basedata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PointRepository extends JpaRepository<Point, Long> {

    @Query(value="select * from point where id in (select id from point_geodata where length(content)<100)", nativeQuery = true)
    List<Point> getItemsWithoutGeodata();

    @Query(value="select * from point where id not in (select id from point_geodata)", nativeQuery = true)
    List<Point> getPointsWithoutGeodataRecord();

    @Query(value="select * from point where latitude is null and googledata is null", nativeQuery = true)
    List<Point> getAllWithoutGoogleDataAndPosition();

    @Query(value="select * from point where id < 10", nativeQuery = true)
    List<Point> getAllDataForTest();

    @Query(value="select * from point where googledata is not null", nativeQuery = true)
    List<Point> getAllWithGoogledata();

    @Query(value="select * from point where gdistance is null", nativeQuery = true)
    List<Point> getAllWithoutDistances();

}
