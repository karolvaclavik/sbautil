package com.pwc.sk.sbautil.model.basedata;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "tp_data", schema = "public")
public class TpData {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "nzd")
    private Double nzd;

    @Column(name = "std")
    private Double std;

    @Column(name = "lod")
    private Double lod;

    @Column(name = "nzt")
    private Double nzt;

    @Column(name = "stt")
    private Double stt;

    @Column(name = "lot")
    private Double lot;

    @Column(name = "address")
    private String address;

    @Column(name = "rawdis")
    private String rawdis;

}
