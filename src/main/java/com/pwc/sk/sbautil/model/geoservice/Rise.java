package com.pwc.sk.sbautil.model.geoservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Rise {

    @JsonProperty("time")
    public int getTime() {
        return this.time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    int time;

    @JsonProperty("astronomical")
    public boolean getAstronomical() {
        return this.astronomical;
    }

    public void setAstronomical(boolean astronomical) {
        this.astronomical = astronomical;
    }

    boolean astronomical;

    @JsonProperty("civil")
    public int getCivil() {
        return this.civil;
    }

    public void setCivil(int civil) {
        this.civil = civil;
    }

    int civil;

    @JsonProperty("nautical")
    public int getNautical() {
        return this.nautical;
    }

    public void setNautical(int nautical) {
        this.nautical = nautical;
    }

    int nautical;

}
