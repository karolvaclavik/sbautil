package com.pwc.sk.sbautil.model.geoservice;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CountryModule {

    @JsonProperty("latitude")
    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    double latitude;

    @JsonProperty("longitude")
    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    double longitude;

    @JsonProperty("common_name")
    public String getCommon_name() {
        return this.common_name;
    }

    public void setCommon_name(String common_name) {
        this.common_name = common_name;
    }

    String common_name;

    @JsonProperty("official_name")
    public String getOfficial_name() {
        return this.official_name;
    }

    public void setOfficial_name(String official_name) {
        this.official_name = official_name;
    }

    String official_name;

    @JsonProperty("capital")
    public String getCapital() {
        return this.capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    String capital;

    @JsonProperty("flag")
    public String getFlag() {
        return this.flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    String flag;

    @JsonProperty("area")
    public int getArea() {
        return this.area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    int area;

    @JsonProperty("landlocked")
    public boolean getLandlocked() {
        return this.landlocked;
    }

    public void setLandlocked(boolean landlocked) {
        this.landlocked = landlocked;
    }

    boolean landlocked;

    @JsonProperty("independent")
    public boolean getIndependent() {
        return this.independent;
    }

    public void setIndependent(boolean independent) {
        this.independent = independent;
    }

    boolean independent;

    @JsonProperty("global")
    public Global getGlobal() {
        return this.global;
    }

    public void setGlobal(Global global) {
        this.global = global;
    }

    Global global;

    @JsonProperty("dial")
    public Dial getDial() {
        return this.dial;
    }

    public void setDial(Dial dial) {
        this.dial = dial;
    }

    Dial dial;

    @JsonProperty("currencies")
    public List<Currency> getCurrencies() {
        return this.currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    List<Currency> currencies;

    @JsonProperty("languages")
    public Languages getLanguages() {
        return this.languages;
    }

    public void setLanguages(Languages languages) {
        this.languages = languages;
    }

    Languages languages;

}
