package com.pwc.sk.sbautil.model.geoservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TimezoneModule {
    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    @JsonProperty("offset_sec")
    public int getOffset_sec() {
        return this.offset_sec;
    }

    public void setOffset_sec(int offset_sec) {
        this.offset_sec = offset_sec;
    }

    int offset_sec;

    @JsonProperty("offset_string")
    public String getOffset_string() {
        return this.offset_string;
    }

    public void setOffset_string(String offset_string) {
        this.offset_string = offset_string;
    }

    String offset_string;
}
