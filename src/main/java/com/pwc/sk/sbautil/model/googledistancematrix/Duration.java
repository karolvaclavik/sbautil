package com.pwc.sk.sbautil.model.googledistancematrix;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Duration {

    @JsonProperty("text")
    String text;

    @JsonProperty("value")
    int value;

}
