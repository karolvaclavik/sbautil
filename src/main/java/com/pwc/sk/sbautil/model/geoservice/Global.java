package com.pwc.sk.sbautil.model.geoservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Global {

    @JsonProperty("alpha2")
    public String getAlpha2() {
        return this.alpha2;
    }

    public void setAlpha2(String alpha2) {
        this.alpha2 = alpha2;
    }

    String alpha2;

    @JsonProperty("alpha3")
    public String getAlpha3() {
        return this.alpha3;
    }

    public void setAlpha3(String alpha3) {
        this.alpha3 = alpha3;
    }

    String alpha3;

    @JsonProperty("numeric_code")
    public String getNumeric_code() {
        return this.numeric_code;
    }

    public void setNumeric_code(String numeric_code) {
        this.numeric_code = numeric_code;
    }

    String numeric_code;

    @JsonProperty("region")
    public String getRegion() {
        return this.region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    String region;

    @JsonProperty("subregion")
    public String getSubregion() {
        return this.subregion;
    }

    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }

    String subregion;

    @JsonProperty("region_code")
    public String getRegion_code() {
        return this.region_code;
    }

    public void setRegion_code(String region_code) {
        this.region_code = region_code;
    }

    String region_code;

    @JsonProperty("subregion_code")
    public String getSubregion_code() {
        return this.subregion_code;
    }

    public void setSubregion_code(String subregion_code) {
        this.subregion_code = subregion_code;
    }

    String subregion_code;

    @JsonProperty("world_region")
    public String getWorld_region() {
        return this.world_region;
    }

    public void setWorld_region(String world_region) {
        this.world_region = world_region;
    }

    String world_region;

    @JsonProperty("continent_name")
    public String getContinent_name() {
        return this.continent_name;
    }

    public void setContinent_name(String continent_name) {
        this.continent_name = continent_name;
    }

    String continent_name;

    @JsonProperty("continent_code")
    public String getContinent_code() {
        return this.continent_code;
    }

    public void setContinent_code(String continent_code) {
        this.continent_code = continent_code;
    }

    String continent_code;

}
