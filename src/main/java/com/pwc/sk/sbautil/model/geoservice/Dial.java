package com.pwc.sk.sbautil.model.geoservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dial {

    @JsonProperty("calling_code")
    public String getCalling_code() {
        return this.calling_code;
    }

    public void setCalling_code(String calling_code) {
        this.calling_code = calling_code;
    }

    String calling_code;

    @JsonProperty("national_prefix")
    public String getNational_prefix() {
        return this.national_prefix;
    }

    public void setNational_prefix(String national_prefix) {
        this.national_prefix = national_prefix;
    }

    String national_prefix;

    @JsonProperty("international_prefix")
    public String getInternational_prefix() {
        return this.international_prefix;
    }

    public void setInternational_prefix(String international_prefix) {
        this.international_prefix = international_prefix;
    }

    String international_prefix;

}
