package com.pwc.sk.sbautil.model.basedata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface TpDataRepository extends JpaRepository<TpData, Long> {

}
