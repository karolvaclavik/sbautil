package com.pwc.sk.sbautil.model.geoservice;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Root {

    @JsonProperty("data")
    public List<Datum> getData() {
        return this.data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    List<Datum> data;

}
