package com.pwc.sk.sbautil.model.geoservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Currency {

    @JsonProperty("symbol")
    public String getSymbol() {
        return this.symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    String symbol;

    @JsonProperty("code")
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    String code;

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    @JsonProperty("numeric")
    public int getNumeric() {
        return this.numeric;
    }

    public void setNumeric(int numeric) {
        this.numeric = numeric;
    }

    int numeric;

    @JsonProperty("minor_unit")
    public int getMinor_unit() {
        return this.minor_unit;
    }

    public void setMinor_unit(int minor_unit) {
        this.minor_unit = minor_unit;
    }

    int minor_unit;

}
