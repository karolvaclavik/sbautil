package com.pwc.sk.sbautil.model.googledistancematrix;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class GoogleDistanceMatrix {

    @JsonProperty("destination_addresses")
    List<String> destination_addresses;

    @JsonProperty("origin_addresses")
    List<String> origin_addresses;

    @JsonProperty("rows")
    List<Row> rows;

    @JsonProperty("status")
    String status;

}
