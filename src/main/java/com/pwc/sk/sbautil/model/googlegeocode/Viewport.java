package com.pwc.sk.sbautil.model.googlegeocode;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Viewport {

    @JsonProperty("northeast")
    Northeast northeast;

    @JsonProperty("southwest")
    Southwest southwest;

}
