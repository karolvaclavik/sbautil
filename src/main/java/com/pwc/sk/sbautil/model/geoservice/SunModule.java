package com.pwc.sk.sbautil.model.geoservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SunModule {

    @JsonProperty("rise")
    public Rise getRise() {
        return this.rise;
    }

    public void setRise(Rise rise) {
        this.rise = rise;
    }

    Rise rise;

    @JsonProperty("set")
    public Set getSet() {
        return this.set;
    }

    public void setSet(Set set) {
        this.set = set;
    }

    Set set;

    @JsonProperty("transit")
    public int getTransit() {
        return this.transit;
    }

    public void setTransit(int transit) {
        this.transit = transit;
    }

    int transit;

}
