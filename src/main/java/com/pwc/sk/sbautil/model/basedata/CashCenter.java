package com.pwc.sk.sbautil.model.basedata;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "cash_center", schema = "public")
public class CashCenter {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "name")
    private String name;

}
