package com.pwc.sk.sbautil.model.optimization;

import com.pwc.sk.sbautil.model.basedata.CashCenter;
import lombok.Data;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.InverseRelationShadowVariable;

import java.util.ArrayList;
import java.util.List;

@PlanningEntity
public class Center {

    private long id;
    private long capacity;
    private long setupCost;
    private CashCenter cc;

    @InverseRelationShadowVariable(sourceVariableName = "center")
    private List<CashPoint> cashPoints = new ArrayList<>();

    public Center() {

    }

    public long getSetupCost() {
        return this.setupCost;
    }

    public long getCapacity() {
        return this.getCapacity();
    }

    public double getUsedCapacity() {
        double result = 0;
        for (CashPoint p : this.cashPoints) {
            result = result + p.getDemand();
        }
        return result;
    }

    public boolean isUsed() {
        return !cashPoints.isEmpty();
    }

    public Center(long id, long capacity, long setupCost, CashCenter cc) {
        this.id = id;
        this.capacity = capacity;
        this.setupCost = setupCost;
        this.cc = cc;
    }

    public double getCapacityPenalty() {
        double result = 0;
        for (CashPoint cp : cashPoints) {
            result = result + cp.getDemand();
        }
        return this.capacity - result;
    }

}
