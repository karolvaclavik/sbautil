package com.pwc.sk.sbautil.core.geolocation;

import com.pwc.sk.sbautil.model.basedata.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VariantCalculator {

    private final ExportRepository exportRepository;
    private ExportdRepository exportdRepository;

    private List<Export> exports;
    private List<Exportd> exportds;

    public VariantCalculator(ExportRepository exportRepository, ExportdRepository exportdRepository) {
        this.exportRepository = exportRepository;
        this.exports = exportRepository.findAll();
        this.exportds = exportdRepository.findAll();
    }

    private double compute(Variant variant, Export export) {
        double result = 1000000;

        if (variant.getC1()!=0 && result>export.getT1()) {
            result = export.getT1();
        }

        if (variant.getC2()!=0 && result>export.getT2()) {
            result = export.getT2();
        }

        if (variant.getC3()!=0 && result>export.getT3()) {
            result = export.getT3();
        }

        if (variant.getC4()!=0 && result>export.getT4()) {
            result = export.getT4();
        }

        if (variant.getC5()!=0 && result>export.getT5()) {
            result = export.getT5();
        }

        if (variant.getC6()!=0 && result>export.getT6()) {
            result = export.getT6();
        }

        if (variant.getC7()!=0 && result>export.getT7()) {
            result = export.getT7();
        }

        if (variant.getC8()!=0 && result>export.getT8()) {
            result = export.getT8();
        }

        if (variant.getC9()!=0 && result>export.getT9()) {
            result = export.getT9();
        }

        return result;
    }

    private double computed(Variant variant, Exportd export) {
        double result = 10000000;

        if (variant.getC1()!=0 && result>export.getT1()) {
            result = export.getT1();
        }

        if (variant.getC2()!=0 && result>export.getT2()) {
            result = export.getT2();
        }

        if (variant.getC3()!=0 && result>export.getT3()) {
            result = export.getT3();
        }

        if (variant.getC4()!=0 && result>export.getT4()) {
            result = export.getT4();
        }

        if (variant.getC5()!=0 && result>export.getT5()) {
            result = export.getT5();
        }

        if (variant.getC6()!=0 && result>export.getT6()) {
            result = export.getT6();
        }

        if (variant.getC7()!=0 && result>export.getT7()) {
            result = export.getT7();
        }

        if (variant.getC8()!=0 && result>export.getT8()) {
            result = export.getT8();
        }

        if (variant.getC9()!=0 && result>export.getT9()) {
            result = export.getT9();
        }

        return result;
    }

    public double calculateVariant(Variant variant) {
        double result = 0;
        for (Export export : exports) {
            System.out.println(export.getId());
            result = result + compute(variant,export);
        }
        return result;
    }

    public double calculateVariantDistance(Variant variant) {
        double result = 0;
        for (Exportd exportd : exportds) {
            System.out.println(exportd.getId());
            result = result + computed(variant,exportd);
        }
        return result;
    }


}
