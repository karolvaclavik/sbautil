package com.pwc.sk.sbautil.core.optimization;

import com.pwc.sk.sbautil.model.basedata.CashCenter;
import com.pwc.sk.sbautil.model.basedata.CashCenterRepository;
import com.pwc.sk.sbautil.model.basedata.Point;
import com.pwc.sk.sbautil.model.basedata.PointRepository;
import com.pwc.sk.sbautil.model.optimization.CashPoint;
import com.pwc.sk.sbautil.model.optimization.Center;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class DataProvider {

    PointRepository pointRepository;
    CashCenterRepository cashCenterRepository;
    DistanceProvider distanceProvider;

    ArrayList<Center> centers;
    ArrayList<CashPoint> cashPoints;

    public void fillData() {
        cashPoints = new ArrayList<>();
        for (Point point : pointRepository.findAll()) {
            //setting demand to 1 for now
            cashPoints.add(new CashPoint(point.getId(),1, point, distanceProvider));
        }

        centers = new ArrayList<>();
        for (CashCenter c : cashCenterRepository.findAll()) {
            //setting capacity to 750 for now
            centers.add(new Center(c.getId(),750, 2000000, c));
        }
    }

    public DataProvider(
            PointRepository pointRepository,
            CashCenterRepository cashCenterRepository,
            DistanceProvider distanceProvider
    ) {
        this.pointRepository = pointRepository;
        this.cashCenterRepository = cashCenterRepository;
        this.fillData();
    }

    public void showValues() {
        System.out.println("Centers:"+centers.size());
        System.out.println("CashPoints:"+cashPoints.size());
    }

    public ArrayList<Center> getCenters(){
        return this.centers;
    }

    public ArrayList<CashPoint> getCashPoints() {
        return this.cashPoints;
    }

}
