package com.pwc.sk.sbautil.core.optimization;

import com.pwc.sk.sbautil.model.basedata.CashCenter;
import com.pwc.sk.sbautil.model.basedata.Dist;
import com.pwc.sk.sbautil.model.basedata.DistRepository;
import com.pwc.sk.sbautil.model.basedata.Point;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class DistanceProvider {

    DistRepository distRepository;

    HashMap<Long, HashMap<Long, Long>> data;

    public DistanceProvider(
            DistRepository distRepository
    ) {
        this.distRepository = distRepository;
        this.fillData();
    }

    public void fillData() {
        this.data = new HashMap<>();
        for (Dist dist : distRepository.findAll()) {
            HashMap<Long, Long> ccMap = this.data.get(dist.getCc());
            if (ccMap==null) {
                ccMap = new HashMap<>();
                this.data.put(dist.getCc(),ccMap);
            }
            ccMap.put(dist.getPoint(),Math.round(dist.getDistance()));
        }
    }

    public long getDistance(CashCenter cc, Point p) {
        HashMap<Long,Long> ccmap = this.data.get(cc.getId());
        if (ccmap==null) {
            return 1000000000; //there is no connection between the two
        }
        Long distance = ccmap.get(p.getId());
        if (distance==null) {
            return 1000000000;
        }
        return distance;
    }

}
