package com.pwc.sk.sbautil.core.geolocation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.io.ByteSource;
import com.pwc.sk.sbautil.model.geoservice.Geolocation;
import com.pwc.sk.sbautil.model.geoservice.Root;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

@Component
public class Geoservice {

    //public final String destinations = "48.167522,17.150429|48.146842,17.125041|48.149954,17.872944|48.736454,19.161472|49.220935,18.741276|48.721043,21.259788|47.990992,18.162888|49.049943,20.300151|49.215292,18.741293";
    public final String destinations = "48.167522,17.150429|48.146842,17.125041|48.149954,17.872944|48.736454,19.161472|49.220935,18.741276|48.721043,21.259788|47.990992,18.162888|49.049943,20.300151|49.215292,18.741293";
    public final String destinationstp = "47.987301,18.165396|49.152678,16.504224|49.422507,17.722085";

    public Geolocation geotag(String address) throws IOException {
        String encodedAddress = URLEncoder.encode(address, "UTF-8");
        URL url = new URL("http://api.positionstack.com/v1/forward?access_key=7b71be9c55e56686497d6b438320039a&query="+encodedAddress);
        System.out.println(url.getQuery());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty("accept", "application/json");
        InputStream responseStream = connection.getInputStream();
        System.out.println("Code:"+connection.getResponseCode());

        ByteSource byteSource = new ByteSource() {
            @Override
            public InputStream openStream() throws IOException {
                return responseStream;
            }
        };

        String text = byteSource.asCharSource(Charsets.UTF_8).read();

        ObjectMapper objectMapper = new ObjectMapper();
        Root res = objectMapper.readValue(text,Root.class);

        if (res!=null & !res.getData().isEmpty()) {
            return new Geolocation(res.getData().get(0).getLatitude(),res.getData().get(0).getLongitude(),text);
        } else {
            return null;
        }

    }

    public Geolocation geotagGoogleRaw(String address) throws IOException {
        String apiKey="AIzaSyD5EySdE1ZM7Y1N0nCQ8PjOV7wivWzivdE";
        String encodedAddress = URLEncoder.encode(address, "UTF-8");
        URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address="+encodedAddress+"&key="+apiKey);
        System.out.println(url.getQuery());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty("accept", "application/json");
        InputStream responseStream = connection.getInputStream();
        System.out.println("Code:"+connection.getResponseCode());

        ByteSource byteSource = new ByteSource() {
            @Override
            public InputStream openStream() throws IOException {
                return responseStream;
            }
        };

        String text = byteSource.asCharSource(Charsets.UTF_8).read();

        ObjectMapper objectMapper = new ObjectMapper();
        //Root res = objectMapper.readValue(text,Root.class);

        System.out.println(text);
        return new Geolocation(0,0,text);

    }

    public Geolocation geotagRaw(String address) throws IOException {
        String encodedAddress = URLEncoder.encode(address, "UTF-8");
        URL url = new URL("http://api.positionstack.com/v1/forward?access_key=7b71be9c55e56686497d6b438320039a&query="+encodedAddress);
        System.out.println(url.getQuery());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty("accept", "application/json");
        InputStream responseStream = connection.getInputStream();
        System.out.println("Code:"+connection.getResponseCode());

        ByteSource byteSource = new ByteSource() {
            @Override
            public InputStream openStream() throws IOException {
                return responseStream;
            }
        };

        String text = byteSource.asCharSource(Charsets.UTF_8).read();

        ObjectMapper objectMapper = new ObjectMapper();
        //Root res = objectMapper.readValue(text,Root.class);

        System.out.println(text);
        return new Geolocation(0,0,text);
    }

    public String geotagGoogleRawToString(String address) throws IOException {
        String apiKey="AIzaSyD5EySdE1ZM7Y1N0nCQ8PjOV7wivWzivdE";
        String encodedAddress = URLEncoder.encode(address, "UTF-8");
        URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address="+encodedAddress+"&key="+apiKey);
        System.out.println(url.getQuery());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty("accept", "application/json");
        InputStream responseStream = connection.getInputStream();
        System.out.println("Code:"+connection.getResponseCode());

        ByteSource byteSource = new ByteSource() {
            @Override
            public InputStream openStream() throws IOException {
                return responseStream;
            }
        };

        String text = byteSource.asCharSource(Charsets.UTF_8).read();

        ObjectMapper objectMapper = new ObjectMapper();
        //Root res = objectMapper.readValue(text,Root.class);

        System.out.println(text);
        return text;
    }


    public String getDistancesTp(double latitude, double longitude) throws IOException {
        String apiKey="AIzaSyD5EySdE1ZM7Y1N0nCQ8PjOV7wivWzivdE";
        String or = latitude + "," + longitude;
        String origins = URLEncoder.encode(or, "UTF-8");
        String encodedDestinations = URLEncoder.encode(destinationstp, "UTF-8");
        URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+origins+"&destinations="+encodedDestinations+"&key="+apiKey);

        System.out.println(url.getQuery());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty("accept", "application/json");
        InputStream responseStream = connection.getInputStream();
        System.out.println("Code:"+connection.getResponseCode());

        ByteSource byteSource = new ByteSource() {
            @Override
            public InputStream openStream() throws IOException {
                return responseStream;
            }
        };

        String text = byteSource.asCharSource(Charsets.UTF_8).read();
        System.out.println(text);
        return text;
    }

    public String getDistances(double latitude, double longitude) throws IOException {
        //String apiKey = "AIzaSyAetNfeCL2b0lwiiu3ML-lGZbiYb2dT5EI";
        String apiKey="NONONO";
        String or = latitude + "," + longitude;
        String origins = URLEncoder.encode(or, "UTF-8");
        String encodedDestinations = URLEncoder.encode(destinations, "UTF-8");
        URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+origins+"&destinations="+encodedDestinations+"&key="+apiKey);

        System.out.println(url.getQuery());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty("accept", "application/json");
        InputStream responseStream = connection.getInputStream();
        System.out.println("Code:"+connection.getResponseCode());

        ByteSource byteSource = new ByteSource() {
            @Override
            public InputStream openStream() throws IOException {
                return responseStream;
            }
        };

        String text = byteSource.asCharSource(Charsets.UTF_8).read();
        System.out.println(text);
        return text;
    }


}
