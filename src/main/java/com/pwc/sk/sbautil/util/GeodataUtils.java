package com.pwc.sk.sbautil.util;

import com.pwc.sk.sbautil.model.geoservice.Datum;
import com.pwc.sk.sbautil.model.geoservice.Root;

public class GeodataUtils {

    public static double maxSvkConfidence(Root data) {
        double result = -1;
        for (Datum dt : data.getData()) {
            if (dt.getCountry().equalsIgnoreCase("Slovakia")) {
                if (dt.getConfidence()>result) {
                    result = dt.getConfidence();
                }
            }
        }
        return result;
    }

    private static boolean isRelevant(Datum dt) {
        if (dt==null) {
            return false;
        }
        if (dt.getCountry().equalsIgnoreCase("Slovakia")) {
            return true;
        }
        return false;
    }

    private static boolean isBetter(Datum old, Datum nw) {
        if (old==null) {
            return true;
        }
        if (nw.getConfidence()>old.getConfidence()) {
            return true;
        } else {
            return false;
        }
    }

    public static Datum getBestSlovak(Root data) {
        Datum result = null;
        if (data==null & data.getData().isEmpty()) {
            return null;
        }
        for (Datum d : data.getData()) {
            if (isRelevant(d)) {
                if (isBetter(result,d)) {
                    result = d;
                }
            }
        }
        return result;
    }

}
