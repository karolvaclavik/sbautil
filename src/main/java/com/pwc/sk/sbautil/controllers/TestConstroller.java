package com.pwc.sk.sbautil.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pwc.sk.sbautil.core.geolocation.Geoservice;
import com.pwc.sk.sbautil.core.geolocation.VariantCalculator;
import com.pwc.sk.sbautil.model.basedata.*;
import com.pwc.sk.sbautil.model.geoservice.Datum;
import com.pwc.sk.sbautil.model.geoservice.Geolocation;
import com.pwc.sk.sbautil.model.geoservice.Root;


import com.pwc.sk.sbautil.model.googledistancematrix.Element;
import com.pwc.sk.sbautil.model.googledistancematrix.GoogleDistanceMatrix;
import com.pwc.sk.sbautil.model.googlegeocode.GoogleGeocodeRoot;
import com.pwc.sk.sbautil.util.GeodataUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Controller
public class TestConstroller {

    private PointRepository pointRepository;
    private PointGeodataRepository pointGeodataRepository;
    private Geoservice geoservice;
    private DistRepository distRepository;
    private VariantRepository variantRepository;
    private VariantCalculator variantCalculator;
    private TpDataRepository tpDataRepository;

    public TestConstroller(
            PointRepository pointRepository,
            PointGeodataRepository pointGeodataRepository,
            Geoservice geoservice,
            DistRepository distRepository,
            VariantRepository variantRepository,
            VariantCalculator variantCalculator,
            TpDataRepository tpDataRepository
    ) {
        this.pointRepository = pointRepository;
        this.pointGeodataRepository = pointGeodataRepository;
        this.geoservice = geoservice;
        this.distRepository = distRepository;
        this.variantRepository = variantRepository;
        this.variantCalculator = variantCalculator;
        this.tpDataRepository = tpDataRepository;
    }

    @GetMapping("/tpgeotag")
    public String tpDataGeotag(Model model) throws IOException {
        ArrayList<TpData> toSave = new ArrayList<>();
        for (TpData tp : tpDataRepository.findAll()) {
            String gl = geoservice.geotagGoogleRawToString(tp.getName()+", Slovakia");
            tp.setAddress(gl);
            toSave.add(tp);
        }
        tpDataRepository.saveAll(toSave);
        return "ok";
    }

    @GetMapping("/tpextract")
    public String tpDataExtract(Model model) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        ArrayList<TpData> toSave = new ArrayList<>();
        for (TpData tp : tpDataRepository.findAll()) {
            GoogleGeocodeRoot g = om.readValue(tp.getAddress(),GoogleGeocodeRoot.class);
            tp.setLatitude(g.getResults().get(0).getGeometry().getLocation().getLat());
            tp.setLongitude(g.getResults().get(0).getGeometry().getLocation().getLng());
            toSave.add(tp);
        }
        tpDataRepository.saveAll(toSave);
        return "ok";
    }

    @GetMapping("/tpdist")
    public String tpDataDistances(Model model) throws IOException {
        ArrayList<TpData> toSave = new ArrayList<>();
        for (TpData tp : tpDataRepository.findAll()) {
            String resp = geoservice.getDistancesTp(tp.getLatitude(),tp.getLongitude());
            tp.setRawdis(resp);
            toSave.add(tp);
        }
        tpDataRepository.saveAll(toSave);
        return "ok";
    }

    @GetMapping("/tpdistextract")
    public String tpDataDistanceExtract(Model model) throws JsonProcessingException {
        ArrayList<TpData> toSave = new ArrayList<>();
        ObjectMapper om = new ObjectMapper();
        for (TpData tp : tpDataRepository.findAll()) {
            GoogleDistanceMatrix dm = om.readValue(tp.getRawdis(),GoogleDistanceMatrix.class);
            tp.setNzd(new Double(dm.getRows().get(0).getElements().get(0).getDistance().getValue()));
            tp.setStd(new Double(dm.getRows().get(0).getElements().get(1).getDistance().getValue()));
            tp.setLod(new Double(dm.getRows().get(0).getElements().get(2).getDistance().getValue()));

            tp.setNzt(new Double(dm.getRows().get(0).getElements().get(0).getDuration().getValue()));
            tp.setStt(new Double(dm.getRows().get(0).getElements().get(1).getDuration().getValue()));
            tp.setLot(new Double(dm.getRows().get(0).getElements().get(2).getDuration().getValue()));

            toSave.add(tp);
        }
        tpDataRepository.saveAll(toSave);
        return "ok";
    }

    @GetMapping("/geotagtest")
    public String geotagTest(Model model) throws IOException {
        String address = "Lánska 928/7, Považská Bystrica, Slovakia";
        Geolocation response = geoservice.geotag(address);
        System.out.println("Response:" + response);
        return "ok";
    }

    private String pointToAddress(Point point) {
        if (point == null) {
            return null;
        }
        String address = point.getStreet();
        if (!address.isEmpty()) {
            address = address + ", ";
        }
        address = address + point.getCity() + ", Slovakia";
        return address;
    }

    @GetMapping("/parsecontent")
    public String fillConfidence(Model model) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<PointGeodata> geodataList = pointGeodataRepository.findAll();
        int c = 0;
        for (PointGeodata pg : geodataList) {
            c++;
            if (c % 100 ==0) {
                System.out.println(c);
            }
            try {
                if (pg.getContent().length()>100) {
                    Root res = objectMapper.readValue(pg.getContent(),Root.class);
                    Datum dt = GeodataUtils.getBestSlovak(res);
                    if (dt!=null) {
                        pg.setConfidence(dt.getConfidence());
                        pg.setCountry(dt.getCountry());
                        pg.setSvkconfidence(dt.getConfidence());
                        pg.setLongitude(dt.getLongitude());
                        pg.setLatitude(dt.getLatitude());
                        pg.setType(dt.getType());
                    } else {
                        pg.setConfidence(null);
                        pg.setCountry(null);
                        pg.setSvkconfidence(null);
                        pg.setLongitude(null);
                        pg.setLatitude(null);
                        pg.setType(null);
                    }
                }
            } catch (Exception e) {

            }
        }
        pointGeodataRepository.saveAll(geodataList);
        return "ok";
    }

    @GetMapping("/play")
    public String troubleshootingrange(Model model) throws JsonProcessingException {
        PointGeodata pg = pointGeodataRepository.findById(2524l).get();
        ObjectMapper objectMapper = new ObjectMapper();
        Root res = objectMapper.readValue(pg.getContent(),Root.class);
        System.out.println(pg.getContent());
        return "ok";
    }


    @GetMapping("/fillmissing")
    public String listAll(Model model) throws InterruptedException {
        double nullcount = 0;
        ArrayList<Point> toSave = new ArrayList<>();
        for (Point point : pointRepository.findAll()) {
            if (point.getLatitude() == null) {
                String address = point.getStreet();
                if (!address.isEmpty()) {
                    address = address + ", ";
                }
                address = address + point.getCity() + ", Slovakia";
                Geolocation gl = null;
                try {
                    gl = geoservice.geotag(address);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (gl != null) {
                    point.setLatitude(gl.getLatitude());
                    point.setLongitude(gl.getLongitude());
                    toSave.add(point);
                }
                nullcount++;
                TimeUnit.MILLISECONDS.sleep(200);
            }
        }
        System.out.println("Saving items: " + toSave.size());
        pointRepository.saveAll(toSave);
        pointRepository.flush();
        model.addAttribute("nullcount", nullcount);
        return "ok";
    }

    @GetMapping("/getrawdata")
    public String getRawData(Model model) throws InterruptedException {
        ArrayList<PointGeodata> toSave = new ArrayList<>();
        int c = 0;
        for (Point point : pointRepository.getItemsWithoutGeodata()) {
            c++;
            String address = point.getStreet();
            if (!address.isEmpty()) {
                address = address + ", ";
            }
            address = address + point.getCity() + ", Slovakia";
            Geolocation gl = null;
            try {
                gl = geoservice.geotagRaw(address);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (gl != null) {
                PointGeodata newData = new PointGeodata(point.getId(),gl.getRawData());
                toSave.add(newData);
            }
            TimeUnit.MILLISECONDS.sleep(700);
            System.out.println(c);
        }
        System.out.println("Saving items: " + toSave.size());
        pointGeodataRepository.saveAll(toSave);
        pointGeodataRepository.flush();
        return "ok";
    }

    @GetMapping("/createmissingGeodata")
    public String createMissingGeodata(Model model) throws InterruptedException {
        ArrayList<PointGeodata> toSave = new ArrayList<>();
        int c = 0;
        for (Point point : pointRepository.getPointsWithoutGeodataRecord()) {
            c++;
            String address = point.getStreet();
            if (!address.isEmpty()) {
                address = address + ", ";
            }
            address = address + point.getCity() + ", Slovakia";
            Geolocation gl = null;
            try {
                gl = geoservice.geotagRaw(address);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (gl != null) {
                PointGeodata newData = new PointGeodata(point.getId(),gl.getRawData());
                toSave.add(newData);
            }
            TimeUnit.MILLISECONDS.sleep(700);
            System.out.println(c);
        }
        System.out.println("Saving items: " + toSave.size());
        pointGeodataRepository.saveAll(toSave);
        pointGeodataRepository.flush();
        return "ok";
    }

    @GetMapping("/getgooglemissing")
    public String getGoogleMissingData() throws IOException {
        List<Point> dt = pointRepository.getAllWithoutGoogleDataAndPosition();
        int c=0;
        for (Point point : dt) {
            c++;
            System.out.println(c);
            String address = pointToAddress(point);
            if (address!=null) {
                Geolocation gl = geoservice.geotagGoogleRaw(address);
                if (gl!=null) {
                    point.setGoogleData(gl.getRawData());
                }
            }
        }
        pointRepository.saveAll(dt);
        pointRepository.flush();
        System.out.println("Done writing "+dt.size()+" records");
        return "ok";
    }

    @GetMapping("/parsegoogledata")
    public String parseGoogleData() throws JsonProcessingException {
        List<Point> dt = pointRepository.getAllWithGoogledata();

        ObjectMapper om = new ObjectMapper();

        for (Point p : dt) {
            try {
                GoogleGeocodeRoot googleData = om.readValue(p.getGoogleData(),GoogleGeocodeRoot.class);
                if (googleData!=null) {
                    p.setLatitude(googleData.getResults().get(0).getGeometry().getLocation().getLat());
                    p.setLongitude(googleData.getResults().get(0).getGeometry().getLocation().getLng());
                }
            } catch (Exception e) {
                System.out.println("ID:"+p.getId());
                e.printStackTrace();
            }
        }
        pointRepository.saveAll(dt);
        pointRepository.flush();
        return "ok";
    }

    /*
    @GetMapping("/getDistances")
    public String getGoogleDistances() throws IOException {
        List<Point> points = pointRepository.getAllWithoutDistances();
        for (Point point : points) {
            System.out.println(point.getId());
            String dt = geoservice.getDistances(point.getLatitude(), point.getLongitude());
            point.setGdistance(dt);
            pointRepository.save(point);
            pointRepository.flush();
        }
        return "ok";
    }*/

    @GetMapping("/parseDistance")
    public String parseGoogleDistance() throws JsonProcessingException {
        List<Point> points = pointRepository.findAll();
        ObjectMapper om = new ObjectMapper();
        int c = 0;
        ArrayList<Dist> toSave = new ArrayList<>();
        for (Point point : points) {
            System.out.println(c++);
            System.out.println(point.getId());
            GoogleDistanceMatrix gd  = om.readValue(point.getGdistance(),GoogleDistanceMatrix.class);
            long cnt = 0;
            if (point.getId()==658) {
                System.out.println("Break");
            }
            for (Element e : gd.getRows().get(0).getElements()) {
                cnt++;
                if (e.getStatus().equalsIgnoreCase("OK")) {
                    Dist d = new Dist();
                    d.setPoint(point.getId());
                    d.setCc(cnt);
                    d.setDistance((double) e.getDistance().getValue());
                    d.setDistanceText(e.getDistance().getText());
                    d.setTime((double) e.getDuration().getValue());
                    d.setTimeText(e.getDuration().getText());
                    toSave.add(d);
                }
            }
            System.out.println(gd.getOrigin_addresses().get(0));
        }
        distRepository.saveAll(toSave);
        distRepository.flush();
        return "ok";
    }

    @GetMapping("/parsegoogleaddress")
    public String parseGoogleAddress() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        ArrayList<Point> toSave = new ArrayList<>();
        for (Point point : pointRepository.findAll()) {
            try {
                GoogleDistanceMatrix gdm = om.readValue(point.getGdistance(),GoogleDistanceMatrix.class);
                if (gdm!=null) {
                    point.setGadress(gdm.getOrigin_addresses().get(0));
                    toSave.add(point);
                }
            } catch (Exception e) {

            }
        }
        pointRepository.saveAll(toSave);
        pointRepository.flush();
        return "ok";
    }


    @GetMapping("/fillvariants")
    public String calculateVariants() {
        List<Variant> toSave = variantRepository.findAll();
        for (Variant variant : toSave) {
            double d = variantCalculator.calculateVariant(variant);
            double dd = variantCalculator.calculateVariantDistance(variant);
            variant.setTotal(d);
            variant.setDistance(dd);
        }
        variantRepository.saveAll(toSave);
        return "ok";
    }


}
