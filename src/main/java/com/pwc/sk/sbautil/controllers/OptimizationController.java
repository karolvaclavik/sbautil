package com.pwc.sk.sbautil.controllers;

import com.pwc.sk.sbautil.core.optimization.DataProvider;
import com.pwc.sk.sbautil.core.optimization.DistanceProvider;
import com.pwc.sk.sbautil.optaplanner.domain.FacilityLocationProblem;
import org.optaplanner.core.api.solver.SolverJob;
import org.optaplanner.core.api.solver.SolverManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Controller
public class OptimizationController {

    DataProvider dataProvider;
    DistanceProvider distanceProvider;
    FacilityLocationProblem facilityLocationProblem;

    //@Autowired
    private SolverManager<FacilityLocationProblem, UUID> solverManager;


    public OptimizationController(
            DataProvider dataProvider,
            DistanceProvider distanceProvider,
            FacilityLocationProblem facilityLocationProblem,
            SolverManager<FacilityLocationProblem, UUID> solverManager
    ) {
        this.dataProvider = dataProvider;
        this.distanceProvider = distanceProvider;
        this.facilityLocationProblem = facilityLocationProblem;
        this.solverManager = solverManager;
    }

    @GetMapping("/optimize")
    public String optimize() {
        UUID problemId = UUID.randomUUID();
        FacilityLocationProblem fsp = new FacilityLocationProblem(dataProvider.getCenters(),dataProvider.getCashPoints());
        // Submit the problem to start solving
        SolverJob<FacilityLocationProblem, UUID> solverJob = solverManager.solve(problemId, fsp);
        FacilityLocationProblem solution;
        try {
            // Wait until the solving ends
            solution = solverJob.getFinalBestSolution();
        } catch (InterruptedException | ExecutionException e) {
            throw new IllegalStateException("Solving failed.", e);
        }
        System.out.println("x");
        return "ok";
    }

}
