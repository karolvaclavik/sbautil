create table tp_data
(
    id bigint primary key ,
    name character varying,
    latitude double precision,
    longitude double precision,
    nzd double precision,
    std double precision,
    lod double precision,
    nzt double precision,
    stt double precision,
    lot double precision
)